// There's no command in this repository. Instead, it contains three different
// license files. One is
//
//	github.com-google-licensecheck-licenses-BSD-3-Clause
//
// copied from [0]. This one will be referred to as "the template" later on.
// The next is
//
//	go.googlesource.com-go-LICENSE
//
// copied from [1]. This one will be called "the Go license" later on. Finally
//
//	modernc.org-ql-LICENSE
//
// copied from [2]. This one will be called "the QL license".
//
// Tools used
//
// To produce/reproduce the observations bellow, use the unlicensable tool by
// Dan Kortschak, found at [3] and diff (GNU diffutils) 3.6.
//
// Initial observations
//
// Passing a name of a license file to the unlicensable tool in turns passes that file content
// to the licensecheck[4] package and prints the resulting licensecheck.Coverage slice.
//
// Let's check what the licensecheck package has to say about the template:
//
//	$ unlicensable github.com-google-licensecheck-licenses-BSD-3-Clause
//	[{path:github.com-google-licensecheck-licenses-BSD-3-Clause cover:{Percent:100 Match:[{Name:BSD-3-Clause Type:2 Percent:100 Start:1 End:1427 IsURL:false}]}}]
//	$
//
// The license type is detected correctly and it is a 100% match. Trying the Go license:
//
//	$ unlicensable go.googlesource.com-go-LICENSE
//	[{path:go.googlesource.com-go-LICENSE cover:{Percent:100 Match:[{Name:BSD-3-Clause Type:2 Percent:98.13084112149532 Start:1 End:1421 IsURL:false}]}}]
//	$
//
// The license type is detected correctly again and it is a 100% match. Trying the QL license:
//
//	$ unlicensable modernc.org-ql-LICENSE
//	[{path:modernc.org-ql-LICENSE cover:{Percent:94.83568075117371 Match:[{Name:BSD-2-Clause Type:2 Percent:36.95652173913044 Start:1 End:481 IsURL:false} {Name:BSD-3-Clause Type:2 Percent:62.149532710280376 Start:542 End:1422 IsURL:false}]}}]
//	$
//
// The overall match score of nearly 95% doesn't sound bad, does it? The actual
// threshold or even the algorithm used by pkg.go.dev to interpret the results
// from the licensecheck package is unknown because pkg.go.dev is closed
// source. What _is_ known, is that this license does not pass the license
// policy check[5] as evidenced at [6], where the API documentation is hidden
// with the message “Doc” not displayed due to license restrictions.
//
// Is the the QL license actually that much different with respect to the Go
// license?
//
//	$ diff -u modernc.org-ql-LICENSE go.googlesource.com-go-LICENSE
//	--- modernc.org-ql-LICENSE	2020-02-01 14:27:42.483378563 +0100
//	+++ go.googlesource.com-go-LICENSE	2020-02-01 14:27:42.483378563 +0100
//	@@ -1,4 +1,4 @@
//	-Copyright (c) 2014 The ql Authors. All rights reserved.
//	+Copyright (c) 2009 The Go Authors. All rights reserved.
//
//	 Redistribution and use in source and binary forms, with or without
//	 modification, are permitted provided that the following conditions are
//	@@ -10,7 +10,7 @@
//	 copyright notice, this list of conditions and the following disclaimer
//	 in the documentation and/or other materials provided with the
//	 distribution.
//	-   * Neither the names of the authors nor the names of the
//	+   * Neither the name of Google Inc. nor the names of its
//	 contributors may be used to endorse or promote products derived from
//	 this software without specific prior written permission.
//
//	$
//
// The diff reveals there are two differing lines. The first one differs in the
// date and the entity name that holds the copyright.  The other line differs
// in not naming Google but generic "authors". It could be argued that these
// differences are at the minimum of what's possible while being clear about
// the work was done by someone else than Google. Okay, if even the minimal
// changes trip pkg.go.dev, let's fix the licensecheck package, right?
//
// The first conclusion was wrong
//
// While trying to figure out what's broken in the licensecheck package, one
// can notice there are two items in the Match slice for the QL license, but
// only one in all other cases. Long story short, there's one more subtle
// difference between the Go license and the QL license that I was not able to
// notice for some time. In the second differing line, the last word also
// differs, for purely grammar reasons. At least that was my intent as a
// non-native speaker.
//
// I am not a lawyer, but I guess that changing a single word/item in a
// license, which is not about the entity or date, actually _has_ to be flagged
// as an incompatible change - to stay on the safe side while possibly
// deferring to some manual override by a human referee. On the other hand, a
// single word change in the middle of a license should probably not lead to
// detecting it as a start of one license type and the tail of a different one.
//
// The fix
//
// Trying the fixed version of the QL license:
//
//	$ unlicensable modernc.org-ql-LICENSE-fixed
//	[{path:modernc.org-ql-LICENSE-fixed cover:{Percent:100 Match:[{Name:BSD-3-Clause Type:2 Percent:97.19626168224299 Start:1 End:1422 IsURL:false}]}}]
//	$
//
// And here's the patch:
//
//	$ diff -u modernc.org-ql-LICENSE-fixed modernc.org-ql-LICENSE
//	--- modernc.org-ql-LICENSE-fixed	2020-02-01 16:57:28.200881690 +0100
//	+++ modernc.org-ql-LICENSE	2020-02-01 16:26:04.563014870 +0100
//	@@ -10,7 +10,7 @@
//	 copyright notice, this list of conditions and the following disclaimer
//	 in the documentation and/or other materials provided with the
//	 distribution.
//	-   * Neither the names of the authors nor the names of its
//	+   * Neither the names of the authors nor the names of the
//	 contributors may be used to endorse or promote products derived from
//	 this software without specific prior written permission.
//
//	$
//
// Note the different last words of the +- lines.
//
// The license policy actually had the correct solution
//
// Quoting [5]:
//
//	If you are a package author who believes a license for one of your
//	packages should have been detected and was not, please check for
//	discrepancies between your license and the OSI text.
//
// The above advice just worked in this case. The problem was in spotting the
// invisible for the always hurrying reader difference in 'the' vs 'its' only
// after having it right before one's eyes for the Nth time, N being some tens
// of times.
//
// There seems to be more problems related to the license policy, but this text
// was concerned with scratching my own itch only.
//
// License
//
// This text was written in hope it may help other people that are also
// frustrated by the inferior presentation of their work at pkg.do.dev because
// of a very small deviation from an otherwise accepted license text.
//
// The author disclaims copyright to this text.
//
// Links
//
// Referenced from elsewhere
//
//  [0]: https://github.com/google/licensecheck/blob/e43db20c11150f4881b8a1c61a34f3ccd896a9f7/licenses/BSD-3-Clause.
//
//  [1]: git clone https://go.googlesource.com/go at
//
//	commit 866920a07351ad5663cc712c31a0f7b5631bc85c (HEAD -> master, origin/master, origin/HEAD)
//	Author: Filippo Valsorda <filippo@golang.org>
//	Date:   Fri Jan 31 15:41:03 2020 -0800
//
//  [2]: https://gitlab.com/cznic/ql/blob/9d9d88bcad8f33e8355baf078ce0678e243ef76d/LICENSE
//
//  [3]: https://github.com/kortschak/unlicensable at
//
//	commit ef7a2619acda6f153521a743bc820dc6d56f1bfa (HEAD -> master, origin/master, origin/HEAD)
//	Author: Dan Kortschak <dan@kortschak.io>
//	Date:   Sat Dec 21 09:01:47 2019 +1030
//
//  [4]: https://github.com/google/licensecheck
//
//  [5]: https://pkg.go.dev/license-policy
//
//  [6]: https://pkg.go.dev/modernc.org/ql?tab=doc
package main
